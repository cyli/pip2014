#!/bin/bash

set -e;
set -x;

rm -fr Pipify.app;
open -W Pipify.platypus;
tar cvzf Pipify.app.tgz Pipify.app;
scp Pipify.app.tgz bootstrap.sh web@pip2014.com:site/glyph.im/pip/;
scp index.html web@pip2014.com:site/pip2014.com/;
